//////////////////////////////////////////////////////////////////////////////
/// @file test_stack.cpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the test_stack implementation file
//////////////////////////////////////////////////////////////////////////////


#include "test_stack.h"
#include <iostream>
#include <cmath>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION (Test_stack);

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_constructor ();
/// @brief This tests the constructor of the Stack class
/// @pre Stack class should already be declared. 
/// This function will instantiate a Stack and make sure the initial 
/// values are correct (size is zero, m_front is null).
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_stack::test_constructor ()
{  
	//cout << endl << "Testing creation of Stack...";
	Stack <int> a;
	CPPUNIT_ASSERT(a.size () == 0);//size of list is zero
	CPPUNIT_ASSERT(a.empty() == true);//front is null -- it's empty
	//cout << endl << "Success!";
	//cout << endl;  
}


//////////////////////////////////////////////////////////////////////////////
/// @fn void test_copy_constructor ();
/// @brief This tests the copy constructor of the Stack class
/// @pre Stack class should already be declared. 
/// This function will instantiate an Stack and set all the values in it to 
/// be equal to that of the parameter value
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_stack::test_copy_constructor()
{
	//cout << endl << "Prepping for copy constructor test...";
	const unsigned int TEST_MAX = 5000;
 
	//cout << endl << "Testing empty copy constructor...";
	Stack <int> a;
	//Yes, I'm testing the empty case. So sue me.
	Stack <int> b(a);
    
	CPPUNIT_ASSERT (a.size() == b.size());
	CPPUNIT_ASSERT (a.empty() == true);
	CPPUNIT_ASSERT (b.empty() == true);
	try
	{
		a.pop();
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
	}
	try
	{
		b.pop();
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
	}
  
	//Testing one element in list...
	//cout << endl << "Testing single copy constructor...";
	Stack <int> c;
	c.push(9);//Push in one element
  
	Stack <int> d(c);//copy
    
	CPPUNIT_ASSERT (c.size() == d.size());
  
	CPPUNIT_ASSERT (c.top() == 9);
	CPPUNIT_ASSERT (d.top() == 9);
  
	//And now for the big kahuna...
	//cout << endl << "Testing multiple element copy constructor...";
	Stack <int> e;
	for (unsigned int i = 0; i < TEST_MAX; i++)
	{
		e.push(i);
	}
  
	Stack <int> f(e);
  
	CPPUNIT_ASSERT (e.size() == f.size());
	for(int i = TEST_MAX; i > 0; i--)
	{
		CPPUNIT_ASSERT(e.size() == i);
		CPPUNIT_ASSERT(f.size() == i);
		CPPUNIT_ASSERT(e.top() == i-1);
		CPPUNIT_ASSERT(f.top() == i-1);
		
		e.pop();
		f.pop();
	}
  
	//Copy constructor success!
	//cout << endl << "Success!";
	//cout << endl;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_assignment ();
/// @brief This tests the assignment operator of the Stack class
/// @pre Stack class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_stack::test_assignment()
{
//cout << endl << "Prepping for assignment test...";
	const unsigned int TEST_MAX = 5000;
 
	//cout << endl << "Testing empty assignment...";
	Stack <int> a;
	//Yes, I'm testing the empty case. So sue me.
	Stack <int> b = a;
    
	CPPUNIT_ASSERT (a.size() == b.size());
	CPPUNIT_ASSERT (a.empty() == true);
	CPPUNIT_ASSERT (b.empty() == true);
	try
	{
		a.pop();
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
	}
	try
	{
		b.pop();
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
	}
  
	//Testing one element in list...
	//cout << endl << "Testing single assignment...";
	Stack <int> c;
	c.push(9);//Push in one element
  
	Stack <int> d = c;//copy
    
	CPPUNIT_ASSERT (c.size() == d.size());
  
	CPPUNIT_ASSERT (c.top() == 9);
	CPPUNIT_ASSERT (d.top() == 9);
  
	//And now for the big kahuna...
	//cout << endl << "Testing multiple element assignment...";
	Stack <int> e;
	for (unsigned int i = 0; i < TEST_MAX; i++)
	{
		e.push(i);
	}
  
	Stack <int> f = e;
  
	CPPUNIT_ASSERT (e.size() == f.size());
	for(int i = TEST_MAX; i > 0; i--)
	{
		CPPUNIT_ASSERT(e.size() == i);
		CPPUNIT_ASSERT(f.size() == i);
		CPPUNIT_ASSERT(e.top() == i-1);
		CPPUNIT_ASSERT(f.top() == i-1);
		
		e.pop();
		f.pop();
	}
  
	//Copy constructor success!
	//cout << endl << "Success!";
	//cout << endl;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_push ();
/// @brief This tests the push_front function of the Stack class
/// @pre Stack class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_stack::test_push()
{
	const unsigned int TEST_MAX = 5000;

	//cout << endl << "Prepping for push test...";
	Stack <int> a;
	//cout << endl << "Testing push...";

	for(unsigned int i = 0; i < TEST_MAX; i++)
	{
		a.push(i*2);
		CPPUNIT_ASSERT(a.size() == i+1);
		CPPUNIT_ASSERT((a.top()) == (i*2));
	}

	//cout << endl << "Success!";
	//cout << endl;  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_pop ();
/// @brief This tests the pop function of the Stack class
/// @pre Stack class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_stack::test_pop()
{
	//cout << endl << "Prepping for pop...";
	Stack <int> a;
	const int TEST_MAX = 5000;
	
	for(int i = 0; i < TEST_MAX; i++)
	{
	  a.push(i);
	}
	
	
	//cout << endl << "Testing pop...";
	for(int i = TEST_MAX; i > 0; i--)
	{
		CPPUNIT_ASSERT(a.size() == i);
		CPPUNIT_ASSERT(a.top() == i-1);
		a.pop();
	}
		
	try//popping nothing
	{
		a.pop();
		CPPUNIT_ASSERT(0);//If it hits this you screwed up.
	}
	catch(Exception & e)
  {
    CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
  }
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_top ();
/// @brief This tests the top/front function of the Stack class
/// @pre Stack class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_stack::test_top()
{
	//cout << endl << "Prepping for top test...";
	const unsigned int TEST_MAX = 5000;
  
	Stack <int> a;
 
	//cout << endl << "Testing top exception...";
	try
	{ 
		a.top();
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
	}
  
	//cout << endl << "Testing top...";   
	for(unsigned int i = 0; i < TEST_MAX; i++)
	{
		a.push(i);
		CPPUNIT_ASSERT (a.top() == i); 
	}
  
	//cout << endl << "Success!";
	//cout << endl;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_clear ();
/// @brief This tests the clear function of the Stack class
/// @pre Stack class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_stack::test_clear()
{
	//cout << endl << "Prepping for clear test...";
  const unsigned int TEST_MAX = 5000;
  
	Stack <int> a;
   
	a.push(1);
  
  
  CPPUNIT_ASSERT (!a.empty());//It isn't empty
  
  //cout << endl << "Testing clear...";   
  a.clear();//CLEAR!!
  
  CPPUNIT_ASSERT (a.empty());
  
  //cout << endl << "Success!";
  //cout << endl;  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_size ();
/// @brief This tests the size function of the Stack class
/// @pre Stack class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_stack::test_size()
{
  //cout << endl << "Prepping for size test...";//No jokes, please...
  const unsigned int TEST_MAX = 5000;
  
  Stack <int> a;
  
  
  //cout << endl << "Testing size...";   
  CPPUNIT_ASSERT(a.size() == 0);
  
  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
	a.push(i);
	
  }
    
  a.clear();
  
  CPPUNIT_ASSERT(a.size() == 0);

  //cout << endl << "Success!";
  //cout << endl;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_empty ();
/// @brief This tests the empty function of the Stack class
/// @pre Stack class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_stack::test_empty()
{
	//cout << endl << "Prepping for empty test...";
  const unsigned int TEST_MAX = 5000;
  
  Stack <int> a;
    
  //cout << endl << "Testing empty...";   
  
  CPPUNIT_ASSERT (a.empty());
  
  a.push(4);
  
  CPPUNIT_ASSERT (!a.empty());
  
  a.clear();
  
  CPPUNIT_ASSERT (a.empty());
  
  //cout << endl << "Success!";
  //cout << endl;
}

