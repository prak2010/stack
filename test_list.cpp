//////////////////////////////////////////////////////////////////////////////
/// @file test_list.cpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the test_list implementation file
//////////////////////////////////////////////////////////////////////////////

#include "test_list.h"
#include <iostream>
#include <cmath>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION (Test_list);

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_constructor ();
/// @brief This tests the constructor of the List class
/// @pre List class should already be declared. 
/// This function will instantiate an List and make sure the initial 
/// values are correct (size is zero, m_front and m_back are null).
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_constructor()
{  
  //cout << endl << "Testing creation of list...";
  List <int> a;
  CPPUNIT_ASSERT(a.size () == 0);//size of list is zero
  CPPUNIT_ASSERT(a.empty() == true);//front and back are null -- it's empty
	try
	{
	  a.front();
	}
	catch(Exception & e)
	{
		//Make sure it threw the exception
	  CPPUNIT_ASSERT(CONTAINER_EMPTY == e.error_code ());
	}
  //cout << endl << "Success!";
  //cout << endl;  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_copy_constructor ();
/// @brief This tests the copy constructor of the List class
/// @pre List class should already be declared. 
/// This function will instantiate an List and set all the values in it to 
/// be equal to that of the parameter value
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_copy_constructor()
{
  //cout << endl << "Prepping for copy constructor test...";
  const unsigned int TEST_MAX = 5000;

  //cout << endl << "Testing empty copy constructor...";
  List <int> a;
  
  //Yes, I'm testing the empty case. So sue me.
  List <int> b(a);
  
  ListIterator<int> u;//Create iterator
  ListIterator<int> v;//Create another iterator
  
  CPPUNIT_ASSERT (a.size() == b.size());
  
  for (u = a.begin (), v = b.begin (); u != a.end (); u++, v++)
  {
    CPPUNIT_ASSERT (*u == *v);
  }
  
  
  //Testing one element in list...
  //cout << endl << "Testing single copy constructor...";
  List <int> c;
  c.push_front(9);//Push in one element
  
  List <int> d(c);//copy
  
  ListIterator<int> w;//Create iterator
  ListIterator<int> x;//Create another iterator
  
  CPPUNIT_ASSERT (c.size() == d.size());
  
  for (w = c.begin (), x = d.begin (); w != c.end (); w++, x++)
  {
    CPPUNIT_ASSERT (*w == *x);
  }
  
  //And now for the big kahuna...
  //cout << endl << "Testing multiple element copy constructor...";
  List <int> e;
  for (unsigned int i = 0; i < TEST_MAX; i++)
  {
    e.push_front(i);
  }
  
  List <int> f(e);
  
  ListIterator<int> y;//Create iterator
  ListIterator<int> z;//Create another iterator
  
  CPPUNIT_ASSERT (e.size() == f.size());
  for(y = e.begin (), z = f.begin (); y != e.end (); y++, z++)
  {
    CPPUNIT_ASSERT (*y == *z);
  }
  
  //Copy constructor success!
  //cout << endl << "Success!";
  //cout << endl;  
  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_assignment ();
/// @brief This tests the assignment operator of the List class
/// @pre List class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_assignment()
{
  //cout << endl << "Prepping for assignment test...";
  const unsigned int TEST_MAX = 5000;
 
  //cout << endl << "Testing empty list assignment...";
  List <int> a;
  //Yes, I'm testing the empty case. So sue me.
  List <int> b;
  
  b = a;
    
  ListIterator<int> u;//Create iterator
  ListIterator<int> v;//Create another iterator
  
  CPPUNIT_ASSERT (a.size() == b.size());
  
  for (u = a.begin (), v = b.begin (); u != a.end (); u++, v++)
  {
    CPPUNIT_ASSERT (*u == *v);
  }
  
  
  //Testing one element in list...
  //cout << endl << "Testing single element List assignment...";
  List <int> c;
  c.push_front(9);//Push in one element
  
  List <int> d;
  d = c;//copy
  
  ListIterator<int> w;//Create iterator
  ListIterator<int> x;//Create another iterator
  
  CPPUNIT_ASSERT (c.size() == d.size());
  
  for (w = c.begin (), x = d.begin (); w != c.end (); w++, x++)
  {
    CPPUNIT_ASSERT (*w == *x);
  }
  
  //And now for the big kahuna...
  //cout << endl << "Testing multiple element List assignment...";
  List <int> e;
  for (unsigned int i = 0; i < TEST_MAX; i++)
  {
    e.push_front(i);
  }
  
  List <int> f;
  
  f = e;//And here I thought f=ma...
  
  ListIterator<int> y;//Create iterator
  ListIterator<int> z;//Create another iterator
  
  CPPUNIT_ASSERT (e.size() == f.size());
  for(y = e.begin (), z = f.begin (); y != e.end (); y++, z++)
  {
    CPPUNIT_ASSERT (*y == *z);
  }
  

  //cout << endl << "Success!";
  //cout << endl;  
}


//////////////////////////////////////////////////////////////////////////////
/// @fn void test_push_front ();
/// @brief This tests the push_front function of the List class
/// @pre List class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_push_front()
{
  //cout << endl << "Prepping for push front...";
	List <int> a;
	const int TEST_MAX = 5000;
	
	//cout << endl << "Testing empty push front...";
	CPPUNIT_ASSERT(a.size () == 0);//size of list is zero
	try
	{
	  a.front();
	}
	catch(Exception & e)
	{
		//Make sure it threw the exception
	  CPPUNIT_ASSERT(CONTAINER_EMPTY == e.error_code ());
	}
	
	//cout << endl << "Testing push front one item...";
	a.push_front(0);
	CPPUNIT_ASSERT(a.size () == 1);//size of list is one
	CPPUNIT_ASSERT(a.front() == 0);//Element is 0
	
	//cout << endl << "Testing push front item two...";
	a.push_front(1);
	CPPUNIT_ASSERT(a.size () == 2);//size of list is one
	CPPUNIT_ASSERT(a.front() == 1);//Element is 0
	
	//cout << endl << "Testing massive push front...";
	for(int i = 2; i < TEST_MAX; i++)
	{
	  a.push_front(i);
		CPPUNIT_ASSERT(a.size () == i+1);//size of list is one greater than i
		CPPUNIT_ASSERT(a.front() == i);//Element is i
	}
	
	//cout << endl << "Success!";
  //cout << endl;  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_pop_front ();
/// @brief This tests the pop_front function of the List class
/// @pre List class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_pop_front()
{
	//cout << endl << "Prepping for pop front...";
	List <int> a;
	const int TEST_MAX = 5000;
	
	for(int i = 0; i < TEST_MAX; i++)
	{
	  a.push_front(i);
	}
	
	
	//cout << endl << "Testing pop front...";
	for(int i = TEST_MAX; i > 0; i--)
	{
	  CPPUNIT_ASSERT(a.size() == i);
		CPPUNIT_ASSERT(a.front() == i-1);
		a.pop_front();
	}
		
	try//popping nothing
	{
		a.pop_front();
		CPPUNIT_ASSERT(0);//If it hits this you screwed up.
	}
	catch(Exception & e)
  {
    CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
  }
	//cout << endl << "Success!";
  //cout << endl;  	
}


//////////////////////////////////////////////////////////////////////////////
/// @fn void test_push_back ();
/// @brief This tests the push_back function of the List class
/// @pre List class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_push_back()
{
  //cout << endl << "Prepping for push front...";
	List <int> a;
	const int TEST_MAX = 5000;
	
	//cout << endl << "Testing empty push back...";
	CPPUNIT_ASSERT(a.size () == 0);//size of list is zero
	try
	{
	  a.back();//check the back
	}
	catch(Exception & e)
	{
		//Make sure it threw the exception
	  CPPUNIT_ASSERT(CONTAINER_EMPTY == e.error_code ());
	}
	
	//cout << endl << "Testing push back one item...";
	a.push_back(0);
	CPPUNIT_ASSERT(a.size () == 1);//size of list is one
	CPPUNIT_ASSERT(a.back() == 0);//Element is 0
	
	//cout << endl << "Testing push back item two...";
	a.push_back(1);
	CPPUNIT_ASSERT(a.size () == 2);//size of list is one
	CPPUNIT_ASSERT(a.back() == 1);//Element is 0
	
	//cout << endl << "Testing massive back front...";
	for(int i = 2; i < TEST_MAX; i++)
	{
	  a.push_back(i);
		CPPUNIT_ASSERT(a.size () == i+1);//size of list is one greater than i
		CPPUNIT_ASSERT(a.back() == i);//Element is i
	}
	
	//cout << endl << "Success!";
  //cout << endl;  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_pop_back ();
/// @brief This tests the pop_back function of the List class
/// @pre List class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_pop_back()
{
	//cout << endl << "Prepping for pop back...";
	List <int> a;
	const int TEST_MAX = 5000;
	
	for(int i = 0; i < TEST_MAX; i++)
	{
	  a.push_back(i);
	}
	
	
	//cout << endl << "Testing pop back...";
	for(int i = TEST_MAX; i > 0; i--)
	{
	  CPPUNIT_ASSERT(a.size() == i);
		CPPUNIT_ASSERT(a.back() == i-1);
		a.pop_back();
	}
		
	try//popping nothing
	{
		a.pop_back();
		CPPUNIT_ASSERT(0);//If it hits this you screwed up.
	}
	catch(Exception & e)
  {
    CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
  }
  //cout << endl << "Success!";
  //cout << endl;  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_remove ();
/// @brief This tests the remove function of the List class
/// @pre List class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_remove()
{
  //cout << endl << "Prepping for remove test...";
  const unsigned int TEST_MAX = 5000;
  unsigned int old_size;
  
  List <int> a;
  
   //cout << endl << "Testing empty list remove exception...";
  try//Try to remove from an empty list
  {
    a.remove(5);
		CPPUNIT_ASSERT(0);//If it hits this you screwed up.
  }
  catch(Exception & e)
  {
    CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
  }
 

 //cout << endl << "Further prepping for remove test...";
  //Fill the list
  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    a.push_front(i);
  }
  
  CPPUNIT_ASSERT (a.front() == 4999);
  old_size = a.size();
  
  
  //Remove first item in list
  //cout << endl << "Testing front remove...";
  a.remove(4999);
  
  CPPUNIT_ASSERT (a.front() == 4998);
  CPPUNIT_ASSERT (a.size() == old_size-1);
    
  //Remove item from middle of list
  //cout << endl << "Testing middle remove...";
  a.remove(2000);
  CPPUNIT_ASSERT (a.size() == old_size-2);
  
  //Remove last item in list
  //cout << endl << "Testing back remove...";
  a.remove(0);
  
  CPPUNIT_ASSERT (a.back() == 1);
  CPPUNIT_ASSERT (a.size() == old_size-3);
  
  //Remove item from outside of list -- should cause no change in size.
  //cout << endl << "Testing remove from item not in the SList...";
  try
  {
    a.remove(9000);
  }
  catch(Exception & e)
  {
        CPPUNIT_ASSERT (ITEM_NOT_FOUND == e.error_code ());
		CPPUNIT_ASSERT (a.size() == old_size-3);
  }
  
  //cout << endl << "Success!";
  //cout << endl;  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_front ();
/// @brief This tests the front function of the List class
/// @pre List class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_front()
{
	//cout << endl << "Prepping for front test...";
  const unsigned int TEST_MAX = 5000;
  
  List <int> a;
 
	//cout << endl << "Testing front exception...";
	try
	{ 
		a.front();
		CPPUNIT_ASSERT (0);//You hit this, you stuffed up bad.
	}
  catch(Exception & e)
  {
		CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
  }
  
	//cout << endl << "Testing front...";   
  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    a.push_front(i);
		CPPUNIT_ASSERT (a.front() == i); 
  }
  
  
  
  //cout << endl << "Success!";
  //cout << endl;
  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_back ();
/// @brief This tests the back function of the List class
/// @pre List class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_back()
{
	//cout << endl << "Prepping for back test...";
  const unsigned int TEST_MAX = 5000;
  
  List <int> a;
 
	//cout << endl << "Testing back exception...";
	try
	{ 
		a.back();
		CPPUNIT_ASSERT (0);//You hit this, you stuffed up bad.
	}
  catch(Exception & e)
  {
		CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
  }
  
	//cout << endl << "Testing back...";   
  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    a.push_back(i);
		CPPUNIT_ASSERT (a.back() == i); 
  }
  
  
  
  //cout << endl << "Success!";
  //cout << endl;
  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_clear ();
/// @brief This tests the clear function of the List class
/// @pre List class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_clear()
{
  //cout << endl << "Prepping for clear test...";
  const unsigned int TEST_MAX = 5000;
  
  List <int> a;
  
  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    a.push_front(i);
  }
  
  //cout << endl << "Testing clear...";   
  a.clear();//CLEAR!!
  
  CPPUNIT_ASSERT (a.empty());
  
  //cout << endl << "Success!";
  //cout << endl;
  
  return;
}


//////////////////////////////////////////////////////////////////////////////
/// @fn void test_size ();
/// @brief This tests the size function of the List class
/// @pre List class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_size()
{
  //cout << endl << "Prepping for size test...";//No jokes, please...
  const unsigned int TEST_MAX = 5000;
  
  List <int> a;
  
  
  //cout << endl << "Testing size...";   
  CPPUNIT_ASSERT(a.size() == 0);
  
  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
	a.push_front(i);
	
  }
  
  a.clear();
  
  CPPUNIT_ASSERT(a.size() == 0);

  //cout << endl << "Success!";
  //cout << endl;
  
  return;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_empty ();
/// @brief This tests the empty function of the List class
/// @pre List class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_list::test_empty()
{
  //cout << endl << "Prepping for empty test...";
  const unsigned int TEST_MAX = 5000;
  
  List <int> a;
  
  
  
  //cout << endl << "Testing empty...";   
  
  CPPUNIT_ASSERT (a.empty());
  
  a.push_front(4);
  
  CPPUNIT_ASSERT (!a.empty());
  
  a.clear();
  
  CPPUNIT_ASSERT (a.empty());
  
  //cout << endl << "Success!";
  //cout << endl;
  
  return;
}
