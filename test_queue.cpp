#include "test_queue.h"
#include <iostream>
#include <cmath>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION (Test_queue);

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_constructor ();
/// @brief This tests the constructor of the Queue class
/// @pre Queue class should already be declared. 
/// This function will instantiate a Queue and make sure the initial 
/// values are correct (size is zero, m_front is null).
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_queue::test_constructor()
{
	//cout << endl << "Testing creation of Queue...";
	Queue <int> a;
	CPPUNIT_ASSERT(a.size () == 0);//size of Queue is zero
	CPPUNIT_ASSERT(a.empty() == true);//front and back are null -- it's empty
	try
	{
		a.pop();
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
	}
	//cout << endl << "Success!";
	//cout << endl;  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_copy_constructor ();
/// @brief This tests the copy constructor of the Queue class
/// @pre Queue class should already be declared. 
/// This function will instantiate an Queue and set all the values in it to 
/// be equal to that of the parameter value
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_queue::test_copy_constructor()
{
	//cout << endl << "Prepping for copy constructor test...";
	const unsigned int TEST_MAX = 5000;

	//cout << endl << "Testing empty copy constructor...";
	Queue <int> a;
  
	//Yes, I'm testing the empty case. So sue me.
	Queue <int> b(a);
  
	CPPUNIT_ASSERT (a.size() == b.size());
	CPPUNIT_ASSERT (a.empty() == true);
	CPPUNIT_ASSERT (b.empty() == true);
	
	//And no, there's nothing in there.
	try
	{
		a.pop();
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
	}
	try
	{
		b.pop();
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
	}
  
	//Testing one element in Queue...
	//cout << endl << "Testing single copy constructor...";
	Queue <int> c;
	c.push(9);//Push back one element
  
	Queue <int> d(c);//copy
    
	//Size
	CPPUNIT_ASSERT (c.size() == d.size());
  
	//Front
	CPPUNIT_ASSERT (c.front() == 9);
	CPPUNIT_ASSERT (d.front() == 9);
	
	//Back
	CPPUNIT_ASSERT (c.back() == 9);
	CPPUNIT_ASSERT (d.back() == 9);
  
	//And now for the big kahuna...
	//cout << endl << "Testing multiple element copy constructor...";
	Queue <int> e;
	for (unsigned int i = 0; i < TEST_MAX; i++)
	{
		e.push(i);
	}
  
	Queue <int> f(e);
     
	
	for (int i = 0; i < TEST_MAX; i++)
	{
		CPPUNIT_ASSERT(e.front() == i);
		CPPUNIT_ASSERT(f.front() == i);
		CPPUNIT_ASSERT(e.size() == TEST_MAX-i);
		CPPUNIT_ASSERT(f.size() == TEST_MAX-i);
		e.pop();
		f.pop();		
	}
	
  
  //Copy constructor success!
  //cout << endl << "Success!";
  //cout << endl;  

}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_assignment();
/// @brief This tests the assignment operator of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_queue::test_assignment()
{

	//cout << endl << "Prepping for assignment test...";
	const unsigned int TEST_MAX = 5000;

	//cout << endl << "Testing empty assignment...";
	Queue <int> a;
  
	//Yes, I'm testing the empty case. So sue me.
	Queue <int> b = a;
  
	CPPUNIT_ASSERT (a.size() == b.size());
	CPPUNIT_ASSERT (a.empty() == true);
	CPPUNIT_ASSERT (b.empty() == true);
	
	//And no, there's nothing in there.
	try
	{
		a.pop();
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
	}
	try
	{
		b.pop();
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
	}
  
	//Testing one element in Queue...
	//cout << endl << "Testing single assignment...";
	Queue <int> c;
	c.push(9);//Push back one element
  
	Queue <int> d = c;//copy
    
	//Size
	CPPUNIT_ASSERT (c.size() == d.size());
  
	//Front
	CPPUNIT_ASSERT (c.front() == 9);
	CPPUNIT_ASSERT (d.front() == 9);
	
	//Back
	CPPUNIT_ASSERT (c.back() == 9);
	CPPUNIT_ASSERT (d.back() == 9);
  
	//And now for the big kahuna...
	//cout << endl << "Testing multiple element assignment...";
	Queue <int> e;
	for (unsigned int i = 0; i < TEST_MAX; i++)
	{
		e.push(i);
	}
  
	Queue <int> f = e;
     
	
	for (int i = 0; i < TEST_MAX; i++)
	{
		CPPUNIT_ASSERT(e.front() == i);
		CPPUNIT_ASSERT(f.front() == i);
		CPPUNIT_ASSERT(e.size() == TEST_MAX-i);
		CPPUNIT_ASSERT(f.size() == TEST_MAX-i);
		e.pop();
		f.pop();		
	}
	
  
  //Assignment success!
  //cout << endl << "Success!";
  //cout << endl;  
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_push();
/// @brief This tests the push function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_queue::test_push()
{
	//cout << endl << "Prepping for push front...";
	Queue <int> a;
	const int TEST_MAX = 5000;
	
	//cout << endl << "Testing empty push front...";
	CPPUNIT_ASSERT(a.size () == 0);//size of queue is zero
	try
	{
	  a.front();
	}
	catch(Exception & e)
	{
		//Make sure it threw the exception
		CPPUNIT_ASSERT(CONTAINER_EMPTY == e.error_code ());
	}
	
	//cout << endl << "Testing push of one item...";
	a.push(0);
	CPPUNIT_ASSERT(a.size() == 1);//size of queue is one
	CPPUNIT_ASSERT(a.front() == 0);//Element is 0
	
	//cout << endl << "Testing push of item two...";
	a.push(1);
	CPPUNIT_ASSERT(a.size () == 2);//size of queue is one
	CPPUNIT_ASSERT(a.back() == 1);//Element is 0
	
	//cout << endl << "Testing massive push front...";
	for(int i = 2; i < TEST_MAX; i++)
	{
		a.push(i);
		CPPUNIT_ASSERT(a.size() == i+1);//size of queue is one greater than i
		CPPUNIT_ASSERT(a.back() == i);//Element is i
	}
	
	//cout << endl << "Success!";
	//cout << endl;  

}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_pop();
/// @brief This tests the pop function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_queue::test_pop()
{
	//cout << endl << "Prepping for pop...";
	Queue <int> a;
	const int TEST_MAX = 5000;
	
	for(int i = 0; i < TEST_MAX; i++)
	{
	  a.push(i);
	}
	
	
	//cout << endl << "Testing pop front...";
	for(int i = 0; i < TEST_MAX; i++)
	{
		CPPUNIT_ASSERT(a.size() == TEST_MAX - i);
		CPPUNIT_ASSERT(a.front() == i);
		a.pop();
	}
		
	try//popping nothing
	{
		a.pop_front();
		CPPUNIT_ASSERT(0);//If it hits this you screwed up.
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
	}
	//cout << endl << "Success!";
	//cout << endl;  	
}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_front ();
/// @brief This tests the top/front function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_queue::test_front()
{
	//cout << endl << "Prepping for front test...";
	const unsigned int TEST_MAX = 5000;
  
	Queue <int> a;
 
	//cout << endl << "Testing front exception...";
	try
	{ 
		a.front();
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
	}
  
	//cout << endl << "Testing front...";   
	for(unsigned int i = 0; i < TEST_MAX; i++)
	{
		a.push(i);
		CPPUNIT_ASSERT (a.front() == 0);//Front doesn't change 
	}
  
	//cout << endl << "Success!";
	//cout << endl;
}


//////////////////////////////////////////////////////////////////////////////
/// @fn void test_back ();
/// @brief This tests the back function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_queue::test_back()
{

	//cout << endl << "Prepping for back test...";
	const unsigned int TEST_MAX = 5000;
  
	Queue <int> a;
 
	//Try going back on nothing
	//cout << endl << "Testing back exception...";
	try
	{ 
		a.back();
		CPPUNIT_ASSERT(0); //If you hit this, you screwed up
	}
	catch(Exception & e)
	{
		CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
	}
  
	//cout << endl << "Testing back...";   
	for(unsigned int i = 0; i < TEST_MAX; i++)
	{
		a.push(i);
		CPPUNIT_ASSERT (a.back() == i);
	}
  
	//cout << endl << "Success!";
	//cout << endl;

}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_clear ();
/// @brief This tests the clear function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_queue::test_clear()
{

	//cout << endl << "Prepping for clear test...";
  const unsigned int TEST_MAX = 5000;
  
  Queue <int> a;
  

    a.push(1);
  
  
  //It isn't empty, and it shouldn't be
  CPPUNIT_ASSERT (!a.empty());
  
  //cout << endl << "Testing clear...";   
  a.clear();//CLEAR!!
  
  CPPUNIT_ASSERT (a.empty());//It is empty
  
  //cout << endl << "Success!";
  //cout << endl;  

}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_size ();
/// @brief This tests the size function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_queue::test_size()
{
  //cout << endl << "Prepping for size test...";//No jokes, please...
  const unsigned int TEST_MAX = 5000;
  
  Queue <int> a;
  
  //cout << endl << "Testing size...";   
  //Start at zero
  CPPUNIT_ASSERT(a.size() == 0);
  
  //Fill it, checking size every step of the way
  for(unsigned int i = 0; i < TEST_MAX; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
	a.push(i);
  }
    
	//Blow it away
	a.clear();
  
  //Show that it's empty again.
  CPPUNIT_ASSERT(a.size() == 0);

  //cout << endl << "Success!";
  //cout << endl;

}

//////////////////////////////////////////////////////////////////////////////
/// @fn void test_empty ();
/// @brief This tests the empty function of the Queue class
/// @pre Queue class should already be declared. 
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////
void Test_queue::test_empty()
{
	//cout << endl << "Prepping for empty test...";
  
	Queue <int> a;  
  
	//cout << endl << "Testing empty...";   
 
	//It's currently empty
	CPPUNIT_ASSERT (a.empty());
	CPPUNIT_ASSERT (a.size() == 0);
  
	//Shove something into it.
	a.push(4);
  
	//Now it's not empty
	CPPUNIT_ASSERT (!a.empty());
	CPPUNIT_ASSERT (a.size() != 0);
  
	//Blow it away
	a.clear();
  
	//See, it's empty again
	CPPUNIT_ASSERT (a.empty());
	CPPUNIT_ASSERT (a.size() == 0);
  
	//cout << endl << "Success!";
	//cout << endl;
}

