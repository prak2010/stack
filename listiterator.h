//////////////////////////////////////////////////////////////////////////////
/// @file listiterator.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the header file for the slistiterator class
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn ListIterator ()
/// @brief Constructs the iterator
/// @pre No iterator exists
/// @post Iterator exists
/// @param None (constructor)
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn ListIterator (Node<generic> *)
/// @brief Copy constructor for iterator
/// @pre A previous iterator must exist.
/// @post Current iterator contains the same settings as the old iterator.
/// @param Existing iterator to be copied
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn generic operator* () const
/// @brief Returns value of contents at iterator
/// @pre Iterator and list must exist
/// @post No change
/// @param None
/// @return Returns value of data at current position of iterator
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn ListIterator operator++ ();
/// @brief Moves iterator one step along list
/// @pre List exists, iterator at abitrary point along list
/// @post List exists, iterator one more step along list
/// @param None.
/// @return Nothing
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn SListIterator operator++ (int);
/// @brief Moves iterator indicated number of steps along list
/// @pre List exists, iterator at abitrary point along list
/// @post List exists, iterator 'n' more steps along list
/// @param 'n' steps to take along list
/// @return Nothing
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator== (const SListIterator &) const;
/// @brief Allows you to compare the value of data under iterators
/// @pre Iterators exist
/// @post No change.
/// @param Other iterator to compare to.
/// @return Boolean
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator!= (const SListIterator &) const;
/// @brief Allows you to compare the value of data under iterators
/// @pre Iterators exist
/// @post No change.
/// @param Other iterator to compare to.
/// @return Boolean
////////////////////////////////////////////////////////////////////////////// 

#ifndef LISTITERATOR_H
#define LISTITERATOR_H

#include "node.h"

template <class generic>
class ListIterator
{
  public:
    ListIterator ();
    ListIterator (Node<generic> *);
    generic operator* () const;
    ListIterator operator++ ();
    ListIterator operator++ (int);
    ListIterator operator-- ();
    ListIterator operator-- (int);
    bool operator== (const ListIterator &) const;
    bool operator!= (const ListIterator &) const;

  private:
    Node<generic> * m_current;//The current position in the list
};

#include "listiterator.hpp"
#endif
