//////////////////////////////////////////////////////////////////////////////
/// @file list.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the header file for the implementation of the doubly linked
/// list.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class List
/// @brief Defines all the functions and variables of the List class
/// The List is a Doubly Linked List
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn List ()
/// @brief This is the constructor for the List. It sets the initial size 
/// of the list to zero and points the head (m_front) and tail (m_back) 
/// at NULL.
/// @pre No List exists
/// @post An empty List of size 0 and with m_front and m_back pointing 
/// at NULL exists.
/// @param None (default constructor)
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn List (const List <generic> &)
/// @brief This is the copy constructor. It copies a list (given as a 
/// parameter) directly into the new list. (Note: this is a deep copy)
/// @pre A List exists. 
/// @post A new List exists, an exact copy of the list passed as a parameter.
/// @param The List to be copied.
/// @return None (constructor)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn ~List ()
/// @brief This is the destructor for the List. It calls clear, which just
/// deletes everything in the list using pop_fronts.
/// @pre An List exists
/// @post No List exists (this assists in deleting it.)
/// @param None. (Destructor)
/// @return None. (Destructor)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn List & operator= (const List &)
/// @brief The assignment operator definition for the List class. This puts 
/// everything in list to the right of the operator into the list on the left
/// of the operator. 
/// @pre Two linked lists exist.
/// @post Both lists now contain identical information.
/// @param The list to be set.
/// @param The list to be gotten.
/// @return The List, such that it is identical and independent from the 
/// other SList.
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void push_front (generic)
/// @brief Pushes a new item onto the front of the list.
/// @pre List exists.
/// @post List contains one more element, and size is incremented by one.
/// @param Takes in the element to be added.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void pop_front ()
/// @brief This removes an item from the front of the list. 
/// @pre A List that contains at least one element.
/// @post The List contains one less element, and size is decremented by one.
/// @param None.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void push_back(generic)
/// @brief Pushes a new item onto the back of the list.
/// @pre List exists.
/// @post List contains one more element, and size is incremented by one.
/// @param Takes in the element to be added.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void pop_back ()
/// @brief This removes an item from the back of the list. 
/// @pre A List that contains at least one element.
/// @post The List contains one less element, and size is decremented by one.
/// @param None.
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn void remove (generic)
/// @brief This removes one instance of the passed value from the list.
/// Note that if the passed value is not in the list, remove will throw an 
/// "ITEM_NOT_FOUND" exception.
/// @pre A linked list exists.
/// @post The linked list contains one less instance of the passed value.
/// Size is decremented accordingly.
/// @param Value to be removed
/// @return None (void)
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn generic front () const
/// @brief Returns the value of the first item in the list
/// @pre List with content exists.
/// @post No change. 
/// @param None.
/// @return Returns value of the first item in the list.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn generic back () const
/// @brief Returns the value of the last item in the list
/// @pre List with content exists.
/// @post No change. 
/// @param None.
/// @return Returns value of the last item in the list.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void clear ()
/// @brief Empties the list completely by continuously calling pop_front until
/// the list is empty.
/// @pre List exists
/// @post List has nothing in it, and m_front points to null.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn bool empty () const
/// @brief Returns whether or not the list is empty
/// @pre List exists
/// @post No change
/// @param None
/// @return Boolean telling if the list is empty or not.
////////////////////////////////////////////////////////////////////////////// 

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size () const
/// @brief Returns the current number of elements in the list
/// @pre List exists
/// @post No change.
/// @param None.
/// @return Returns the current number of items in the array (0 - whatever.)
////////////////////////////////////////////////////////////////////////////// 


#ifndef LIST_H
#define LIST_H

#include "listiterator.h"
#include "exception.h"
#include "node.h"

template <class generic>
class List
{
  public:
    List ();
    List (const List <generic> &);
    ~List ();
    List & operator= (const List &);
    void push_front (generic);
    void pop_front ();
    void push_back (generic);
    void pop_back ();
    void remove (generic);
    generic front () const;
    generic back () const;
    void clear ();
    unsigned int size () const;
    bool empty () const;
    typedef ListIterator<generic> Iterator;
    Iterator begin () const;
    Iterator end () const;

  protected:
    unsigned int m_size;
    Node<generic> * m_front;
    Node<generic> * m_back;
};

#include "list.hpp"
#endif
